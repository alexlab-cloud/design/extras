# design.extras

> Secondary assets for the AlexLab Cloud design portfolio

For resources that aren't part of the main AlexLab brand but may be reused in multiple locations.

<div align="center">
    <img src="./assets/ascii-art/alexlab-cloud-logo.ray-so.png" alt="AlexLab Cloud ASCII art logo" width=400 />
</div>

<hr>

<sub>Emoji used for repository logo designed by <a href="https://openmoji.org/">OpenMoji</a> – the open-source emoji and icon project. License: CC BY-SA 4.0</sub>

<hr>
